use clap::Parser;
use create_html::create_html::HtmlCreator;
use std::io::Result;

#[derive(Parser)]
#[command(author, version, about, long_about= None)]
struct Cli {
    dir_name: String,
}

fn main() -> Result<()> {
    let cli = Cli::parse();
    let html_creator = HtmlCreator::new(cli.dir_name);
    html_creator.dir_create()?;
    html_creator.index_html()?;
    Ok(())
}
