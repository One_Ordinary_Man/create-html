pub mod create_html {
    use std::{
        fs,
        io::Result,
        path::{Path, PathBuf},
    };
    pub struct HtmlCreator {
        target_dir: PathBuf,
    }

    impl HtmlCreator {
        pub fn new(target_directory: String) -> HtmlCreator {
            let target_path: PathBuf = PathBuf::from(target_directory);
            HtmlCreator {
                target_dir: target_path,
            }
        }

        pub fn dir_create(&self) -> Result<()> {
            fs::create_dir(self.target_dir.as_path())?;
            Ok(())
        }

        pub fn index_html(&self) -> Result<()> {
            let target_file = Path::join(self.target_dir.as_path(), "index.html");
            let html_file = r#"<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Hello World</title>
    </head>
    <body>
        Hello, World!
    </body>
</html>"#;
            fs::write(target_file, html_file)?;
            Ok(())
        }
    }
}
